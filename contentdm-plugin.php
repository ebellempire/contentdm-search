<?php
/*
Plugin Name: CONTENTdm Search
Plugin URI: http://www.ohiomemory.org/plugin
Description: Search for images on CONTENTdm.
Version: 1.0.0
Author: Ohio Memory
Author URI: http://www.ohiomemory.org/
License: GPL2
*/
?>
<?php
/*
This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License, version 2, as 
published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

include_once 'results-parser-class.php';

class CONTENTdm_Widget extends WP_Widget {

	public function __construct() {
		parent::__construct(
			'contentdm_widget', // Base ID
			'CONTENTdm Search', // Name
			array( 'description' => __( "Search images in CONTENTdm", 'contentdm-search' ), ) // Args
		);
	}

	public function widget( $args, $instance ) {
		// outputs the content of the widget
		extract($args);
		include_once 'widget-content.php';
	}
	
	/*
	public function form( $instance ) {
 		$collection = 'Collection';
 		if ( isset( $instance[ 'collection' ] ) ) {
			$collection = $instance[ 'collection' ];
		}
		?>
		<p>
			
		<label for="<?php echo $this->get_field_id( 'collection' ); ?>">Default collections:</label>
		<input type="text" name="<?php echo $this->get_field_id( 'collection' ); ?>" id="<?php echo $this->get_field_id( 'collection' ); ?>">
		
		</p>
		<?php
	}

	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['collection'] = ( ! empty( $new_instance['collection'] ) ) ? $new_instance['collection'] : '[collection]';

		return $instance;
	}
	*/

}

function contentdm_register_widget() {
	register_widget('CONTENTdm_Widget');
}

add_action('widgets_init', 'contentdm_register_widget');

// Set up the results page 
// - http://wordpress.org/support/topic/how-do-i-create-a-new-page-with-the-plugin-im-building
function contentdm_install() {
	delete_option("contentdm_page_id");
	add_option("contentdm_page_id", '0', '', 'yes');

	$page_title = 'CONTENTdm Search Results';
	$the_page = get_page_by_title( $page_title );

	if ( ! $the_page ) {
		$post = array(
			//'page_template' => [ <template file> ] //Sets the template for the page.
			'comment_status' => 'closed',
			'post_author' => 1,
			'post_content' => 'Some text',
			'post_name' => 'contentdm-search-results',
			'post_status' => 'publish',
			'post_title' => $page_title,
			'post_type' => 'page'
		);  

		// Insert the post into the database
		$the_page_id = wp_insert_post( $post );
	} else {
		// the plugin may have been previously active and the page may just be trashed...
		$the_page_id = $the_page->ID;

		//make sure the page is not trashed...
		$the_page->post_status = 'publish';
		$the_page_id = wp_update_post( $the_page );
	}

	delete_option( 'contentdm_page_id' );
	add_option( 'contentdm_page_id', $the_page_id );
}

register_activation_hook(__FILE__, 'contentdm_install');

function contentdm_remove() {

	//  the id of our page...
	$the_page_id = get_option('contentdm_page_id');
	if( $the_page_id ) {
		wp_delete_post( $the_page_id, true ); // this will delete, not trash
	}

	delete_option("contentdm_page_id");
}

register_deactivation_hook(__FILE__, 'contentdm_remove');

// Replace results page content.
function contentdm_result_page_content($content) {
	global $wp_query;
	$post_id = $wp_query->post->ID;

	if ($post_id == get_option('contentdm_page_id')) {
		include_once 'results.php';
	} else {
		return $content;
	}
}

add_filter("the_content", "contentdm_result_page_content");

// Make sure JS and CSS is included for our results page and the placeholder
// shim for all pages.
function contentdm_page_scripts() {
	global $wp_query;
	$post_id = $wp_query->post->ID;
	$plugin_url = plugins_url().'/contentdm-search/';

	wp_enqueue_script('jquery');
	wp_enqueue_script('jquery.placeholder', $plugin_url.'scripts/jquery.placeholder.min.js', array('jquery'), '1.0.0', true);
	wp_enqueue_script('contentdm_run_placeholder', $plugin_url.'scripts/run-placeholder.js', array('jquery', 'jquery.placeholder'), '1.0.0', true);

	if ($post_id == get_option('contentdm_page_id')) {
		wp_enqueue_script('jquery.shorten', $plugin_url.'scripts/jquery.shorten.js', array('jquery'), '1.0.0', true);
		wp_enqueue_script('contentdm_js', $plugin_url.'scripts/results.js', array('jquery', 'jquery.shorten'), '1.0.0', true);
		wp_enqueue_style('contentdm_css', $plugin_url.'styles/results.css');
	}
}
add_action('wp_enqueue_scripts', 'contentdm_page_scripts');

class CONTENTdmSettingsPage
{
    /**
     * Holds the values to be used in the fields callbacks
     */
    private $options;
    /**
     * Start up
     */
    public function __construct()
    {
        add_action( 'admin_menu', array( $this, 'add_plugin_page' ) );
        add_action( 'admin_init', array( $this, 'page_init' ) );
    }
    /**
     * Add options page
     */
    public function add_plugin_page()
    {
        // This page will be under "Settings"
        add_options_page(
            'CONTENTdm Settings', 
            'CONTENTdm', 
            'manage_options', 
            'cdm-settings-admin', 
            array( $this, 'create_admin_page' )
        );
    }
    /**
     * Options page callback
     */
    public function create_admin_page()
    {
        // Set class property
        $this->options = get_option( 'cdm_option_name' );
        ?>
        <div class="wrap">
            <h2>My Settings</h2>           
            <form method="post" action="options.php">
            <?php
                // This prints out all hidden setting fields
                settings_fields( 'cdm_option_group' );   
                do_settings_sections( 'cdm-setting-admin' );
                submit_button(); 
            ?>
            </form>
        </div>
        <?php
    }
    /**
     * Register and add settings
     */
    public function page_init()
    {        
        register_setting(
            'cdm_option_group', // Option group
            'cdm_option_name', // Option name
            array( $this, 'sanitize' ) // Sanitize
        );

        add_settings_section(
            'cdm_setting_section_id', // ID
            'CONTENTdm Settings', // Title
            array( $this, 'print_section_info' ), // Callback
            'cdm-setting-admin' // Page
        );  

        add_settings_field(
            'cdm-collections', // ID
            'Collection Aliases', // Title 
            array( $this, 'cdm_collections_callback' ), // Callback
            'cdm-setting-admin', // Page
            'cdm_setting_section_id' // Section           
        );      

        add_settings_field(
            'cdm-server', 
            'CONTENTdm Services URL', 
            array( $this, 'cdm_server_callback' ), 
            'cdm-setting-admin', 
            'cdm_setting_section_id'
        );
        
        add_settings_field(
            'cdm-public', 
            'CONTENTdm Public URL', 
            array( $this, 'cdm_public_callback' ), 
            'cdm-setting-admin', 
            'cdm_setting_section_id'
        );       
    }
    
    /**
     * Sanitize each setting field as needed
     *
     * @param array $input Contains all settings fields as array keys
     */
    public function sanitize( $input )
    {
        $new_input = array();
        if( isset( $input['cdm-collections'] ) )
            $new_input['cdm-collections'] = sanitize_text_field( $input['cdm-collections'] );

        if( isset( $input['cdm-server'] ) )
            $new_input['cdm-server'] = sanitize_text_field( $input['cdm-server'] );
       	
       	if( isset( $input['cdm-public'] ) )
            $new_input['cdm-public'] = sanitize_text_field( $input['cdm-public'] );

        return $new_input;
    }

    /** 
     * Print the Section text
     */
    public function print_section_info()
    {
        print 'Enter your settings below:';
    }

    /** 
     * Get the settings option array and print input for each option
     */
    public function cdm_collections_callback()
    {
        printf(
            '<input type="text" id="cdm-collections" name="cdm_option_name[cdm-collections]" value="%s" /> (semicolon delimited list or "all")',
            isset( $this->options['cdm-collections'] ) ? esc_attr( $this->options['cdm-collections']) : ''
        );
    }
    
    public function cdm_server_callback()
    {
        printf(
            '<input type="text" id="cdm-server" name="cdm_option_name[cdm-server]" value="%s" /> (e.g. "https://server12345.contentdm.oclc.org")',
            isset( $this->options['cdm-server'] ) ? esc_attr( $this->options['cdm-server']) : ''
        );
    }
    
    public function cdm_public_callback()
    {
        printf(
            '<input type="text" id="cdm-public" name="cdm_option_name[cdm-public]" value="%s" /> (e.g. "http://cdm12345.contentdm.oclc.org")',
            isset( $this->options['cdm-public'] ) ? esc_attr( $this->options['cdm-public']) : ''
        );
    }
}

if(is_admin()){
	global $pagenow;
	if($pagenow == 'post.php' || $pagenow == 'post-new.php' || $pagenow == 'admin-ajax.php'){
		add_action('admin_enqueue_scripts', 'cdm_search_scripts');
		add_action('admin_head', 'cdm_search_head');
	}
	if($pagenow == 'admin-ajax.php') {
		require_once('cdm_ajax.php');
		add_action('wp_ajax_cdm_search', 'cdm_search_function');
		add_action('wp_ajax_cdm_save_image', 'cdm_save_image_function');
	}

	$cdm_settings_page = new CONTENTdmSettingsPage();
}

// Make sure the results page doesn't show up in menus.
function contentdm_page_filter($pages) {
	$new_pages = array();
	$the_page_id = get_option('contentdm_page_id');

	foreach($pages as $page) {
		if($page->ID != $the_page_id) {
			array_push($new_pages, $page);
		}
	}

	return $new_pages;
}
add_filter('get_pages', 'contentdm_page_filter');
add_action('init', 'contentdm_start_session', 1);
add_action('wp_logout', 'contentdm_end_session');
add_action('wp_login', 'contentdm_end_session');

function contentdm_start_session() {
    if(!session_id()) {
        session_start();
    }
}

function contentdm_end_session() {
    session_destroy ();
}

add_action( 'add_meta_boxes', 'cdm_search_metabox' );
function cdm_search_metabox() {
	$screens = get_post_types();
	foreach($screens as $screen) {
		add_meta_box('cdm_search_section',__( 'CONTENTdm Search', 'contentdm-search' ), 'cdm_search_metabox_content', $screen);
	}
}

function cdm_search_metabox_content($post) {
	
	?>
	
	<div id="cdm_search">
		<div id="search-container">
			<input 
				class="cdm-search-input" 
				id="cdm_keyword" 
				type="text" 
				value="" 
				placeholder="Search" 
				data-control-type="textbox"/>
			
			<span id="searchicon" class="button wp-core-ui">Search</span>
			<span id="waitElem">
				<img src="<?php echo(plugins_url() . "/contentdm-search/") ?>images/spinner.gif" title="Searching...">
			</span>	
		</div>
		<div id="list-total"></div>
		<div id="cdm-results" class="list box text-shadow" style="clear: both;">
    
		</div>	
	</div>
		
<?php 
} 

// Header
function cdm_search_head() {
	global $post;	
	$options = get_option("cdm_option_name");
?>
    <script type="text/javascript">	
		var cdm_collections = '<?php echo str_replace(";", "!", $options["cdm-collections"]); ?>';
		var cdm_server = '<?php echo $options["cdm-server"]; ?>';
		var cdm_public = '<?php echo getenv("COMPUTERNAME") != "PSAGER-XPS" ? $options["cdm-public"] : "http://cdm16007.contentdm.oclc.org"; ?>';
		var cur_post_id = <?php echo $post->ID; ?>; 
		var cdm_plugin_url = '<?php echo plugins_url( '', __FILE__ ); ?>';
		var cdm_security_nonce = {
			security: '<?php echo wp_create_nonce('cdm_security_nonce');?>'
		}		
	</script>
<?php
}

function cdm_search_scripts() {
	// to search, save, insert from CONTENTdm
	wp_register_style( 'cdm-search-css', plugins_url( 'styles/cdm-search-styles.css', __FILE__ ) );
	wp_enqueue_style( 'cdm-search-css' );
	wp_register_script( 'cdm-search-js', plugins_url( 'scripts/cdm-search-js.js', __FILE__ ), array( 'jquery' ) );
	wp_enqueue_script( 'cdm-search-js' );

	// thickbox to display selected images
	wp_enqueue_script( 'thickbox' );
	wp_enqueue_style( 'thickbox' );
}

function cdm_func( $atts ) {
	global $post;	
	$options = get_option("cdm_option_name");
    $a = shortcode_atts( array(
    	'coll' => 'p267401coll32',
        'ptr' => '8356',
    ), $atts );
    $img_link = '<img src=' . $options['cdm-public'] . '"/utils/getthumbnail/collection/' . $atts['coll'] . '/id/' . $atts['ptr'] . '"/>';
    return $img_link;
}
add_shortcode( 'cdm', 'cdm_func' );


?>