<?php

// http://www.ohiomemory.org/wp-content/plugins/contentdm-search/cdm_ajax.php
// $output = cdm_search_function();

function cdm_search_function() {
	
	$max_size = 600;
	if (!empty($_GET["query"])) {
		$query_string = $_GET["query"];
	}
	if (!empty($_POST["query"])) {
		$query_string = $_POST["query"];
	}
	
	//$query_string = 'https://server16007.contentdm.oclc.org/dmwebservices/index.php?q=dmQuery/p267401coll32/CISOSEARCHALL^ulysses^all^and!format^picture^all^and/title!descri!image!imagea/title/50/1/1/0/0/0/json';
	
	$cdm_web_services = preg_replace('/^(http.*q=)(.*)$/', '$1', $query_string);
	$cdm_server = preg_replace('/^https:\/\/server(.*?)\/.*$/', 'cdm$1', $cdm_web_services);
	
	/* e.g. https://server16007.contentdm.oclc.org/dmwebservices/index.php?q=dmQuery/flags!p267401coll32!p267401coll34!p267401coll36/CISOSEARCHALL^murfreesboro^all^and!format^picture^all^and/title!descri!image!imagea/title/50/1/1/0/0/0/json 
	*/
	$cdm_data_json = doCurl($query_string);
	
	$cdm_data_array = json_decode($cdm_data_json);
	$cdm_data_array->pager->total < $cdm_data_array->pager->maxrecs ? $total = $cdm_data_array->pager->total : $total = $cdm_data_array->pager->maxrecs;

	for ($i = 0; $i < $total; $i++) {
		
		// dmGetImageInfo call only necessary if record-level metadata does not have height and width
		if ($cdm_data_array->records[$i]->filetype != 'cpd') {
			
			if (!preg_match('/\d\d.*/', $cdm_data_array->records[$i]->image) || !preg_match('/\d\d.*/', $cdm_data_array->records[$i]->imagea) || ($cdm_data_array->records[$i]->image == $cdm_data_array->records[$i]->imagea)) {
				
				// e.g. https://server16007.contentdm.oclc.org/dmwebservices/index.php?q=dmGetImageInfo/flags/1618/xml
				$image_info_json = doCurl($cdm_web_services . "dmGetImageInfo/" . substr($cdm_data_array->records[$i]->collection, 1) . "/" . $cdm_data_array->records[$i]->pointer . "/xml");
				
				$image_info_array = json_decode($image_info_json);
				
				if (preg_match('/\d\d.*/', $image_info_array->width) && preg_match('/\d\d.*/', $image_info_array->height)) {
					$cdm_data_array->records[$i]->imagea = $image_info_array->width;
					$cdm_data_array->records[$i]->image = $image_info_array->height;
					
				}

			}
			
		} 
		
		else {
			// [ might be better to just skip compound objects. Much less processing involved. ]
		 	$compound_info_json = doCurl($cdm_web_services . "dmGetCompoundObjectInfo/" . substr($cdm_data_array->records[$i]->collection, 1) . "/" . $cdm_data_array->records[$i]->pointer . "/json");
		 	$compound_info_array = json_decode($compound_info_json);
		 	$compound_data_json = doCurl($cdm_web_services . "dmGetImageInfo/" . substr($cdm_data_array->records[$i]->collection, 1) . "/" . $compound_info_array->page[0]->pageptr . "/xml");
		 	$compound_data_array = json_decode($compound_data_json);
		 	$cdm_data_array->records[$i]->pointer = $compound_info_array->page[0]->pageptr;
		 	$cdm_data_array->records[$i]->imagea = $compound_data_array->width;
			$cdm_data_array->records[$i]->image = $compound_data_array->height;
		 	// e.g. http://ohiomemory.org/wp-content/plugins/contentdm-search/cdm_ajax.php?query=https://server16007.contentdm.oclc.org/dmwebservices/index.php?q=dmQuery/p267401coll32/CISOSEARCHALL^Appliqued%20Red%20green^all^and!format^picture^all^and/title!descri!image!imagea/title/50/1/1/0/0/0/json
		 	
		}
		
	}
	
	echo json_encode($cdm_data_array);
	die();

}

function cdm_save_image_alt($src, $post_id, $thumb) {

	$result = media_sideload_image($src, $post_id);
	$attachments = get_posts(array('numberposts' => '1', 'post_parent' => $post_id, 'post_type' => 'attachment', 'post_mime_type' => 'image', 'orderby' => 'post_date', 'order' => 'DESC'));

	if(sizeof($attachments) > 0 && $thumb){
		set_post_thumbnail($post_id, $attachments[0]->ID);	
	}	
	
	$newsrc = wp_get_attachment_image_src( $attachments[0]->ID, "full" );

	if(is_array($newsrc) && !empty($newsrc[0])) {
		return $newsrc[0];
	} else {
		return false;	
	}
	
}

function cdm_save_image_helper($url, $post_id, $thumb = 0, $filename = "", $keyword = "", $title = null, $description = null, $caption=null) {
	
    require_once( ABSPATH . 'wp-admin/includes/file.php' );
    // Download file to temp location, returns full server path to temp file, ex; /home/user/public_html/mysite/wp-content/26192277_640.tmp
    $tmp = download_url( $url . '.jpg' );
	
    if ( is_wp_error( $tmp ) ) {
		@unlink($file_array['tmp_name']);   // clean up
		$file_array['tmp_name'] = '';	

		/*
		$retry = cdm_save_image_alt($url . '.jpg', $post_id, $thumb);
		if($retry == false) {
			$error_string = $tmp->get_error_message();
			return array("error" => $error_string);	
		} else {
			return $newsrc;
		}
		*/
    }
	
    if (!empty($filename)){    // override filename if given, reconstruct server path
		
		$filename = $filename . "_" . date("Y-m-d") . "_" . time();
        $filename = sanitize_file_name($filename) . ".jpg";
        $tmppath = pathinfo( $tmp );                                                   // extract path parts
        $new = $tmppath['dirname'] . "/". $filename;     // build new path
        rename($tmp, $new);                                                            // renames temp file on server
        $tmp = $new;                                                                   // push new filename (in path) to be used in file array later
    }
	
    $file_array['tmp_name'] = $tmp;                                                    // full server path to temp file
	
	$file_array['name'] = $filename;
	
	$desc = $title;
	$post_data['post_content'] = $description;
	$post_data['post_excerpt'] = $caption;

    if ( empty( $post_data['post_parent'] ) ) {
        $post_data['post_parent'] = $post_id;
    }
    
    require_once(ABSPATH . 'wp-admin/includes/file.php');
    require_once(ABSPATH . 'wp-admin/includes/media.php');
    require_once(ABSPATH . 'wp-admin/includes/image.php');
	
	// $post_data can override the items saved to wp_posts table, like post_mime_type, guid, post_parent, post_title, post_content, post_status
    $att_id = media_handle_sideload( $file_array, $post_id, $desc, $post_data );

    if ( is_wp_error($att_id) ) {
		$error_string = $att_id->get_error_message();
        @unlink($file_array['tmp_name']);   // clean up
		return array("error" => $error_string);		
    }

    if ($thumb) {
        set_post_thumbnail($post_id, $att_id);			
    }
	
	$newsrc = wp_get_attachment_image_src( $att_id, "full" );
	
	if ( is_wp_error($newsrc) ) {
		$error_string = $newsrc->get_error_message();
		return array("error" => $error_string);		
	} elseif(is_array($newsrc) && !empty($newsrc[0])) {
		return $newsrc[0];
	} else {
		return array("error" => "Image could not be saved to server.");		
	}
	
}

function cdm_save_image_function() {
	
	$url = $_POST["src"];
	$post_id = $_POST["post_id"];
	$thumb = $_POST["feat_img"];
	$filename = $_POST["filename"];
	$keyword = $_POST["keyword"];
	$title = $_POST["title"];
	$description = $_POST["description"];
	$caption = $_POST["caption"];
	
	//echo json_encode(array($url, $post_id,$filename  ));
    //die();
	
	$nonce = $_POST["wpnonce"];
	if (!wp_verify_nonce($nonce, 'cdm_security_nonce')) {
		echo json_encode(array("error" => "Invalid request."));
		die();
	}		
	
	if(empty($url)) {
		echo json_encode(array("error" => "No image source found."));
		die();	
	}
	
	if(empty($post_id)) {
		echo json_encode(array("error" => "No post found. This feature requires that an auto-save or draft of the current post was saved first."));
		die();	
	}

	$newsrc = cdm_save_image_helper($url, $post_id, $thumb, $filename, $keyword, $title, $description, $caption);

	if(is_array($newsrc)) {
		echo json_encode(array("error" => $newsrc["error"]));
		die();		
	} else {
		echo json_encode(array("result" => $newsrc));
		die();	
	}
	
}
	
function doCurl($query_string) {	
	$ch = curl_init();

	curl_setopt($ch, CURLOPT_URL, $query_string);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
	curl_setopt($ch, CURLOPT_HEADER, 0);  // DO NOT RETURN HTTP HEADERS
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);  // RETURN THE CONTENTS OF THE CALL
	curl_setopt($ch, CURLOPT_TIMEOUT, 240); // same as php.ini max_execution_time
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

	if (!strpos($query_string, 'xml')) {
		$cdm_data_json = curl_exec($ch);
	} else {
		$cdm_data_xml = curl_exec($ch);
		$xml = simplexml_load_string($cdm_data_xml);
		$cdm_data_json = json_encode($xml);
	}
	curl_close($ch);
	return $cdm_data_json;	
}

?>