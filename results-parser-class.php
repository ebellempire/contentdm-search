<?php

class CdmResults {
	private static $bad_chars =  array("‘", "’", "“", "”", "–", "—", "\xe2\x80\xa6", "&#39;");
	private static $good_chars = array("'", "'", '"', '"', '-', '--', '...', "'");
	private static $book_record_def = array(
		array('tag' => 'title', 'bad_chars' => true),
		array('tag' => 'creator', 'bad_chars' => true),
		array('tag' => 'description', 'bad_chars' => true),
		array('tag' => 'date'),
		array('tag' => 'subject'),
		array('tag' => 'collection'),
		array('tag' => 'pointer')
	);
	/*
	private static $get_params_def = array(
		'start',
		'maxrecs'
	);
	*/
	public static $available_sorts = array(
		'date_asc' => 'Year (earliest first)',
		'date_desc' => 'Year (latest first)',
		'creator' => 'Creator',
		'title' => 'Title'
	);
	public static $default_sort = 'title';
	public static $search_criteria_descriptions = array(
		'creator' => 'Creator',
		'title' => 'Title',
		'subject' => 'Subject',
		'published-after' => 'Published after',
		'published-before' => 'Published before'
	);

	//public $results = array();
	public $results;
	public $params = array();
	public $total_matches;
	public $page_count;

	public function __construct($unparsed_results) {
		$this->total_matches = $unparsed_results['pager']['total'];

		if($this->total_matches > 0) {
			$this->as_cdm_records($unparsed_results);

			/*foreach(self::$get_params_def as $param) {
				$this->params[$param] = self::get_tag_contents($unparsed_results, "$param", false);
			}*/
		$this->params['maxrecs'] = $unparsed_results['pager']['maxrecs'];

			if ($this->params['maxrecs'] > 0) {
				$this->page_count = ceil($this->total_matches / $this->params['maxrecs']);
			} else {
				$this->page_count = 0;
			}
		}
	}
	
	private function as_cdm_records($result_list) {
		
		$total = $result_list['pager']['maxrecs'] > $result_list['pager']['total'] ? $result_list['pager']['total'] : $result_list['pager']['maxrecs'];
		
		for ($i = 0; $i < $total; $i++) {
			$alias = preg_replace('@\\/@', '', $result_list['records'][$i]['collection']);
			$ptr = $result_list['records'][$i]['pointer'];
			
			list($creator, $title, $subjects, $description) = self::get_record_contents( $alias, $ptr, true );
			
			$result_list['records'][$i]['creato'] = is_array($creator) ? '' : $creator;
			$result_list['records'][$i]['title'] = is_array($title) ? '' : $title;
			$result_list['records'][$i]['subjec'] = is_array($subjects) ? '' : $subjects;
			$result_list['records'][$i]['descri'] = is_array($description) ? '' : $description;
			$result_list['records'][$i]['largeimg'] = self::get_large_image_url($alias, $ptr);

		}
		$this->results = $result_list;
		//array_push($this->results, $result_list);
		
	}
	
	private static function get_large_image_url($alias, $ptr) {
		
		$img_size = 400;
		// curl to get dimensions and scale to present image in crop window
		$curl_url = contentdm_get('cdm-server') . "/dmwebservices/index.php?q=dmGetImageInfo/" . $alias . "/" . $ptr . "/xml";
		$image_info = self::do_curl($curl_url);
		
		// get width and scale info
		$imgwidth = $image_info['width'];
		$imgheight = $image_info['height'];
		$longest_side = $imgwidth > $imgheight ? $imgwidth : $imgheight;
		$trimmed_scale = "20";
		//$scale = round(($img_size/$longest_side), 2);
		$scale = $img_size/$longest_side;
		$targ_w = $imgwidth*$scale;
		$targ_h = $imgheight*$scale;
		$formatted_scale = sprintf("%01.2f", $scale);
		$trimmed_scale = substr($formatted_scale, 2);
		if ($imgwidth < $img_size) { $trimmed_scale = 100; }
		
		$scaled_link = contentdm_get('cdm-public') . '/utils/ajaxhelper/?CISOROOT=' . $alias . '&CISOPTR=' . $ptr . '&action=2&DMSCALE=' . $trimmed_scale . '&DMWIDTH=' . $targ_w . '&DMHEIGHT=' . $targ_h;
		
		return $scaled_link;	
		
	}
	
	private static function do_curl($curl_url) {
		$ch = curl_init();
	  	curl_setopt($ch, CURLOPT_URL, $curl_url);
	  	curl_setopt($ch, CURLOPT_HEADER, 0);  // DO NOT RETURN HTTP HEADERS
	  	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);  // RETURN THE CONTENTS OF THE CALL
	  	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		if (!strpos($curl_url, 'xml')) {
    		$cdm_data_json = curl_exec($ch);
		} else {
			$cdm_data_xml = curl_exec($ch);
			$xml = simplexml_load_string($cdm_data_xml);
			$cdm_data_json = json_encode($xml);
		}
		curl_close($ch);
		return json_decode($cdm_data_json, true);
	}
	
	private static function get_record_contents($collection, $ptr, $replace_bad_chars) {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, contentdm_get('cdm-server') . "/dmwebservices/index.php?q=dmGetItemInfo/" . $collection . "/" . $ptr . "/json");
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_HEADER, 0);  // DO NOT RETURN HTTP HEADERS
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);  // RETURN THE CONTENTS OF THE CALL
		curl_setopt($ch, CURLOPT_TIMEOUT, 240); // same as php.ini max_execution_time
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		$cdm_data_json = curl_exec($ch);
		curl_close($ch);
  		$working_record_full = json_decode($cdm_data_json, true);
  		$extra_fields = array(
  						$working_record_full['creato'],
  						$working_record_full['title'],
  						$working_record_full['subjec'],
  						$working_record_full['descri']
  						);
  		return $extra_fields;
	}
	
}