<div id="cdm-waiting">Please wait&hellip;</div>

<?php
	
	include_once 'results-parser-class.php';
	//ini_set('display_errors', 'On');
	//error_reporting(E_ALL);
		
	function get_page_id_no() {
		global $post;
		return $post->ID;
	}

	function contentdm_has_criteria() {
		return contentdm_get('creator') != ''
			|| contentdm_get('title') != ''
			|| contentdm_get('keywords') != '';
	}

	function contentdm_get($key) {
		global $post;
		$options = get_option("cdm_option_name");
		$defaults = array(
			'sortby' => isset($_SESSION['contentdm_sortby']) ? $_SESSION['contentdm_sortby'] : CdmResults::$default_sort,
			'start' => 1,
			'maxrecs' => 10,
			'cdm-server' => $options['cdm-server'],
			'cdm-collections' => str_replace(';', '!', $options["cdm-collections"]),
			'cdm-public' => getenv("COMPUTERNAME") != "PSAGER-XPS" ? $options["cdm-public"] : "http://cdm16007.contentdm.oclc.org"
		);
		if(!empty($_GET["cdm-$key"])) {
			return $_GET["cdm-$key"];
		} else if(!empty($defaults[$key])) {
			return $defaults[$key];
		} else {
			return '';
		}
	}

	function get_cdm_url() {
		
		$start = contentdm_get('start');
		if (contentdm_get('page') > contentdm_get('start')) {
			$start = contentdm_get('page') * contentdm_get('maxrecs');	
		}
		return contentdm_get('cdm-server') . "/dmwebservices/index.php?q=dmQuery/" . contentdm_get('cdm-collections') . "/CISOSEARCHALL^" . contentdm_get('keywords') . "^all^and!format^picture^all^and!filetype^cpd^none^and//title/10/" . $start . "/1/0/0/0/json";
		
	}

	function get_cdm_results($tries = 0) {
		$request_result = cdm_remote_get(get_cdm_url());
		if ($request_result instanceof WP_Error) {
			if($tries == 3) {
				// We've already tried three times and it hasn't worked.
				// Display the error.
				return $request_result;
			} else {
				return get_om_results($tries++);
			}
		} else {

			return new CdmResults($request_result);
			$results = new CdmResults($request_result);
			
		}
	}

	$cdm_results = null;

	if(contentdm_has_criteria()) {
		$cdm_results = get_cdm_results();
		//print_r($cdm_results);
		$_SESSION['contentdm_sortby'] = contentdm_get('sortby');
	}
	
	function contentdm_get_reset_url() {
		$to_copy = array(
			'debug'
		);
		$page_params = array(
			'page_id' => get_page_id_no()
		);
		foreach($to_copy as $name) {
			if(contentdm_get($name) != '') {
				$page_params["cdm-$name"] = contentdm_get($name);
			}
		}
		return "?".http_build_query($page_params);
	}
	
	function contentdm_get_page_url($page) {
		$to_copy = array(
			'creator',
			'title',
			'keywords',
			'published-after',
			'published-before',
			'sortby',
			'debug'
		);
		$page_params = array(
			'page_id' => get_page_id_no(),
			'cdm-page' => $page
		);
		foreach($to_copy as $name) {
			if(contentdm_get($name) != '') {
				$page_params["cdm-$name"] = contentdm_get($name);
			}
		}
		return "?".http_build_query($page_params);
	}
	
	function contentdm_page_a($text, $page, $current) {
		$url = esc_url(contentdm_get_page_url($page));
		$class = $page == $current ? 'selected' : '';
		echo "<a href=\"$url\" class=\"$class\">$text</a>";
	}
	
	function contentdm_show_pager($total, $current) {
		if ($total > 1) {
			echo '<div class="cdm-pager">Page:';

			if($current > 1) {
				echo contentdm_page_a('« First', 1, $current);
				echo contentdm_page_a('< Previous', $current - 1, $current);
			}

			$padding = 3;
			$start = max(1, $current - $padding);
			$end = min($total, $current + $padding);

			if($start > 1) {
				echo "<a>&hellip;</a>";
			}

			for($i = $start; $i <= $end; $i++) {
				echo contentdm_page_a($i, $i, $current);
			}

			if($end < $total) {
				echo "<a>&hellip;</a>";
			}

			if($current < $total) {
				echo contentdm_page_a('Next >', $current + 1, $current);
				echo contentdm_page_a('Last »', $total, $current);
			}

			echo '</div>';
		}
	}
	
	function cdm_remote_get($query_string) {

	  $ch = curl_init();

	  curl_setopt($ch, CURLOPT_URL, $query_string);
	  curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
	  curl_setopt($ch, CURLOPT_HEADER, 0);  // DO NOT RETURN HTTP HEADERS
	  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);  // RETURN THE CONTENTS OF THE CALL
	  curl_setopt($ch, CURLOPT_TIMEOUT, 240); // same as php.ini max_execution_time
	  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

	  if (!strpos($query_string, 'xml')) {
	    $cdm_data_json = curl_exec($ch);
	  } else {
	    $cdm_data_xml = curl_exec($ch);
	    $xml = simplexml_load_string($cdm_data_xml);
	    $cdm_data_json = json_encode($xml);
	  }
	  curl_close($ch);
	  return json_decode($cdm_data_json, true);

	}
	
?>

<!--<pre>
	<?php echo (($cdm_results->results['pager']['start'] - 1) * $cdm_results->results['pager']['maxrecs']) + 1; ?>
	<?php echo print_r($cdm_results->results['records'][0]); ?>
</pre>-->
<style>
#myImg {
    border-radius: 5px;
    cursor: pointer;
    transition: 0.3s;
}

#closeImage:hover {opacity: 0.7;}

/* The Modal (background) */
.imgModal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 10001; /* Sit on top */
    padding-top: 100px; /* Location of the box */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
}

/* Modal Content (image) */
.modal-content {
    margin: auto;
    display: block;
    /*width: 80%;*/
    max-width: 400px;
    max-height: 400px;
}

/* Caption of Modal Image */
#caption {
    margin: auto;
    display: block;
    width: 80%;
    max-width: 700px;
    text-align: center;
    color: #ccc;
    padding: 10px 0;
    height: 150px;
}

/* Add Animation */
.modal-content, #caption {    
    -webkit-animation-name: zoom;
    -webkit-animation-duration: 0.6s;
    animation-name: zoom;
    animation-duration: 0.6s;
}

@-webkit-keyframes zoom {
    from {-webkit-transform:scale(0)} 
    to {-webkit-transform:scale(1)}
}

@keyframes zoom {
    from {transform:scale(0)} 
    to {transform:scale(1)}
}

/* The Close Button */
img#closeImage {
    position: absolute;
    top: 15px;
    right: 35px;
    opacity: .5;
    background: none;
    border: none;
    box-shadow: none;
    /*
    color: #f1f1f1;
    font-size: 40px;
    font-weight: bold;
    transition: 0.3s;
    */
    /*
    background-color:#fff;
    color:#7F7F7F;
    padding:20px;
    border:2px solid #ccc;
    -moz-border-radius: 20px;
    -webkit-border-radius:20px;
    -khtml-border-radius:20px;
    -moz-box-shadow: 0 1px 5px #333;
    -webkit-box-shadow: 0 1px 5px #333;
    z-index:101;
    */
}

.close:hover,
.close:focus {
    color: #bbb;
    text-decoration: none;
    cursor: pointer;
}

/* 100% Image Width on Smaller Screens */
@media only screen and (max-width: 700px){
    .modal-content {
        width: 100%;
    }
}
</style>
<script type="text/javascript">
	document.getElementById('cdm-waiting').style.display = 'none';
</script>


<script type="text/javascript">
	function closeModal() {
		
		jQuery(this).parent().css('display', 'none');
		
	}
	jQuery(document).ready(function($) {
		$(".lrgImg").click(function() {
			//alert( "Handler for .click() called." );
			event.preventDefault();
			//$(this).next().show();
			var itemId = $(this).attr('id');
			//$('div#'+itemId).show();
			$('div#'+itemId).css('display', 'block');
			
			
			// Get the modal
			//var modal = document.getElementById('myModal');
			//console.log(modal);
			
			// Get the image and insert it inside the modal - use its "alt" text as a caption
			//var imgLink = document.getElementById('lrgImg');
			//var modalImg = document.getElementById("img01");
			//var captionText = document.getElementById("itemTitle");
			//img.onclick = function(){
			//modal.style.display = "block";
			//modalImg.src = this.href;
			//modalImg.alt = captionText.innerHTML;
			//}
			

			// Get the <span> element that closes the modal
			//var closeImg = document.getElementsByClassName("close")[0];
			// When the user clicks on <span> (x), close the modal
			/*
			$( "#closeImg" ).click(function() { 
				console.log(this);
			    //modal.style.display = "none";
			    //$(this).next().hide();
			    //$('div#'+itemId).hide();
			    //$('div#'+itemId).css('display', 'none');
			    $(this).parent().css('display', 'none');
			});
			*/
		});	
		
		
	});
</script>

<?php if(contentdm_has_criteria()) {
	
	if ($cdm_results instanceof CdmResults) { ?>
		<div class="cdm-criteria">
			<div class="cdm-header">Criteria</div>
			<ul>
				<?php foreach (CdmResults::$search_criteria_descriptions as $key => $description) {
					if (contentdm_get($key) != '') { ?>
						<li>
							<?php echo $description; ?>
							=
							<?php echo esc_html(contentdm_get($key)); ?>
						</li>
					<?php }
				} ?>
			</ul>
			<button onclick="location.href='#cdm-advanced-search';">Advanced Search</a>
		</div>
		<?php if ($cdm_results->total_matches > 0) { ?>
			<div class="cdm-result-count">
				Showing:
				<strong>
					<?php echo (($cdm_results->results['pager']['start'] - 1) * $cdm_results->results['pager']['maxrecs']) + 1; ?>
					-
					<?php echo min($cdm_results->results['pager']['start'] * $cdm_results->results['pager']['maxrecs'], $cdm_results->results['pager']['total']); ?>
				</strong>
				of
				<?php echo $cdm_results->results['pager']['total']; ?>
			</div>
			<?php contentdm_show_pager($cdm_results->page_count, $cdm_results->results['pager']['start']) ?>
			<div class="cdm-results">
				<?php foreach($cdm_results->results['records'] as $result) { ?>
					<div class="cdm-result">
						<div class="cdm-creator">
							<?php echo esc_html($result['creato']) ?>
						</div>
						<div class="cdm-title" id="itemTitle">
							<?php echo esc_html($result['title']) ?>
						</div>
						<div class="cdm-left">
							<div class="cdm-description">
								<?php echo esc_html($result['descri']) ?>
							</div>
						</div>
						<div class="cdm-right">
							
							<!--<div class="cdm-thumbnail">
								<a class="fancybox" href="<?php echo($result['largeimg']); ?>">
									<img src="<?php echo('http://cdm16007.contentdm.oclc.org/utils/getthumbnail/collection'.$result['collection'].'/id/'.$result['pointer']); ?>" alt="Thumbnail image" />
								</a>
							</div>-->
							<a id="<?php echo(substr($result['collection'],1)."-".$result['pointer']); ?>" class="lrgImg" href="<?php echo($result['largeimg']); ?>">
								<img class="myImg" src="<?php echo( contentdm_get('cdm-public') . '/utils/getthumbnail/collection'.$result['collection'].'/id/'.$result['pointer']); ?>" alt="Thumbnail image" />
							</a>
							<!-- The Modal -->
							<div id="<?php echo(substr($result['collection'],1)."-".$result['pointer']); ?>" class="imgModal">
								<!--<span class="close">×</span>-->
								<a id="closeImg" onclick="jQuery(this).parent().css('display', 'none'); return false;">
									<img id="closeImage" src="<?php echo(plugins_url() . "/contentdm-search/") ?>close.png" alt="alt" title="title" />
								</a>
								<img class="modal-content" id="img01" src="<?php echo($result['largeimg']); ?>">
								<div id="caption"></div>
							</div>

						</div>
					</div>
				<?php } ?>
				
			</div>
			<?php contentdm_show_pager($cdm_results->page_count, $cdm_results->results['pager']['start']) ?>
		<?php } else { // No results ?>
			<p>No results found, please try different search terms.</p>
		<?php } ?>
	<?php } else { // Not an CdmResults object, must be an error ?>
		<p>Sorry, your search has encountered an error. Please try again later.</p>
	<?php } 
}?>
<form id="cdm-advanced-search">
	<div class="cdm-header">Advanced Search</div>
	<input name="page_id" value="<?php echo get_option('contentdm_page_id'); ?>" type="hidden" />
	<div class="cdm-advanced-search-form">
		<div class="cdm-control-row">
			<div class="cdm-control-group">
				<label for="cdm-creator">Creator:</label>
				<div class="cdm-controls">
					<input type="text"
						id="cdm-creator"
						name="cdm-creator"
						value="<?php echo esc_attr(contentdm_get('creator')); ?>" />
				</div>
			</div>
			<div class="cdm-control-group">
				<label for="cdm-published-after">Year, minimum:</label>
				<div class="cdm-controls">
					<input type="text"
						id="cdm-published-after"
						name="cdm-published-after"
						class="cdm-smaller"
						value="<?php echo esc_attr(contentdm_get('published-after')); ?>" />
				</div>
			</div>
			<div class="cdm-control-group">
				<label for="cdm-published-before">maximum:</label>
				<div class="cdm-controls">
					<input type="text"
						id="cdm-published-before"
						name="cdm-published-before"
						class="cdm-smaller"
						value="<?php echo esc_attr(contentdm_get('published-before')); ?>" />
				</div>
			</div>
		</div>
		<div class="cdm-control-row">
			<div class="cdm-control-group">
				<label for="cdm-title">Title:</label>
				<div class="cdm-controls">
					<input type="text"
						id="cdm-title"
						name="cdm-title"
						value="<?php echo esc_attr(contentdm_get('title')); ?>" />
				</div>
			</div>
		</div>
		<div class="cdm-control-row">
			<div class="cdm-control-group">
				<label for="cdm-keywords">Keywords:</label>
				<div class="cdm-controls">
					<input type="text"
						id="cdm-keywords"
						name="cdm-keywords"
						value="<?php echo esc_attr(contentdm_get('keywords')); ?>" />
				</div>
			</div>
			<div class="cdm-control-group">
				<label for="cdm-sortby">Sort by:</label>
				<div class="cdm-controls">
					<select name="cdm-sortby" id="cdm-sortby">
						<?php foreach(CdmResults::$available_sorts as $code => $desc) { ?>
							<option value="<?php echo $code; ?>"
								<?php if ($code == contentdm_get('sortby')) { ?>
									selected="selected"
								<?php } ?>>
								<?php echo esc_html($desc); ?>
							</option>
						<?php } ?>
					</select>
				</div>
			</div>
		</div>
	</div>
	<div class="cdm-advanced-search-controls">
		<a href="<?php echo esc_url(contentdm_get_reset_url()); ?>">Reset</a>
		<input type="submit" value="Search" />
		<a href="http://www.ohiomemory.org/">Ohio Memory</a>
	</div>
</form>
<?php if (contentdm_get('debug') == 'true') {
	echo get_cdm_url();
	?>
	<pre>
		<?php echo print_r($cdm_results); ?>
	</pre>
<?php } ?>
