﻿# Image/Metadata insert plugin for CONTENTdm 

This plugin contains two things: a public-side widget for searching CONTENTdm
and an administrative-side search for searching and inserting CONTENTdm images along with 
their title and description to WordPress posts. The primary files that do the work are:

* Administrative search
	1. cdm-search-js.js - starts the search, processes results
	2. cdm_ajax.php - does the actual API work with CONTENTdm

* Public-side search widget
	1. results.php - processes and presents content for the results page
	2. results-parser-class.php - provides results page with object and helper functions
	3. widget-content.php
