<?php
echo $before_widget;
echo $before_title
	. 'Ohio Memory Search'
	. $after_title;
?>
	<style type="text/css">
		.cdm-widget-form {
			overflow: hidden;
		}
		.cdm-field {
			margin-bottom: 0.5em;
		}
		.cdm-field input {
			width: 100%;
			-moz-box-sizing: border-box;
			box-sizing: border-box;
		}
		.cdm-field + input[type='submit'] {
			margin-top: 0.5em;
		}
		.cdm-widget-form img {
			float: right;
			margin-top: 0.5em;
		}
	</style>
	<form class="cdm-widget-form">
		<input name="page_id" value="<?php echo get_option('contentdm_page_id'); ?>" type="hidden" />
		<div class="cdm-field"><input name="cdm-creator" placeholder="Creator" /></div>
		<div class="cdm-field"><input name="cdm-title" placeholder="Title" /></div>
		<div class="cdm-field"><input name="cdm-keywords" placeholder="Keywords" /></div>
		<img src="<?php echo plugins_url(); ?>/contentdm-search/ohiomemory-logo.png"/>
		 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<input type="submit" value="Search" />
		
	</form>
<?php
echo $after_widget;
?>