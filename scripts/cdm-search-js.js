/*! cdm-search-js.js */

jQuery(document).ready(function($) {
	
	if ( $('#waitElem').is(':visible') ) { jQuery('#waitElem').hide(); }
	
	jQuery('#cdm_keyword').keypress(function(e){
        if (e.which == 13) { jQuery('#searchicon').click(); return false; }
    });
	
	jQuery('#searchicon').click(function(e) {
		
		jQuery('#list-total').html('');

		e.preventDefault();
		if ( !$('.list').empty() ) { $('.list').empty(); }
		if ( !$('#waitElem').is(':visible') ) {	jQuery('#waitElem').show();	}
		var keyword = jQuery("#cdm_keyword").val();
		var start = 1;
		var querystring = 	cdm_server + 
							"/dmwebservices/index.php?q=dmQuery/" + 
							cdm_collections + 
							"/CISOSEARCHALL^" + 
							keyword.trim() + 
							"^all^and!format^picture^all^and/title!descri!image!imagea/title/50/" + 
							start + 
							"/1/0/0/0/json";
		jQuery.ajax ({
			type: 'POST',
			url: ajaxurl,
			data: { 'action': 'cdm_search', query: querystring },
			dataType: 'json',
			success: function(results) {
				jQuery('#waitElem').hide();
				processCdmResults(results);
				jQuery('html, body').animate({
				    scrollTop: jQuery("#cdm-results").offset().top
				}, 1000, function() {
				    return false;
				});
			}
		});	
			
	});


	var imgsizes = 	{ 
						thumb: 120, 
						modal: 500, 
						full: 2000 
					};
					
	function getScaledLink(imgwidth, imgheight, imgtype) {
				 
		var maxsize = imgsizes[imgtype];
		var longestside = imgwidth > imgheight ? imgwidth : imgheight;
		var scale = maxsize/longestside;
		var scalepercent = scale * 100;
		var targw = imgwidth * scale;
		var targh = imgheight * scale;
		if (longestside < maxsize) { scale = 100; }
		
		return scaledlink = cdm_public + '/utils/ajaxhelper/?CISOROOT=p267401coll32&CISOPTR=13427&action=2&DMSCALE=' + scalepercent + '&DMWIDTH=' + targw + '&DMHEIGHT=' + targh;
			
	}

	var imgsizes = 	{ 
						thumb: 120, 
						preview: 500, 
						full: 2000 
					};
					
	function getScaledLink(imgwidth, imgheight, imgtype) {
				 
		var maxsize = imgsizes[imgtype];
		var longestside = imgwidth > imgheight ? imgwidth : imgheight;
		var scale = maxsize/longestside;
		var scalepercent = scale * 100;
		var targw = imgwidth * scale;
		var targh = imgheight * scale;
		if (longestside < maxsize) { scale = 100; }
		return { dmscale: scalepercent, dmwidth: targw, dmheight: targh };
			
	}
	
	function processCdmResults(results) {
				
		var total = parseInt(results['pager']['total']) <= parseInt(results['pager']['maxrecs']) ? parseInt(results['pager']['total']) : parseInt(results['pager']['maxrecs']);	
		var maxsize = 2048;
		var entry = "";

		for (var i = 0; i < total; i++) {
			
			var imgheight = parseInt(results['records'][i]['image']);
			var imgwidth = parseInt(results['records'][i]['imagea']);

			var longestside = imgheight;
			var thumbConstraint = ' height="125"';
			if (imgwidth > imgheight) {
				longestside = imgwidth;
				thumbConstraint = ' width="125"';
			} else {
				longestside = imgheight;
				thumbContraint = ' height="125"';
			}

			var itemSourceURL = cdm_public + '/cdm/ref/collection/' + results['records'][i]['collection'].substring(1) + '/id/' + results['records'][i]['pointer'];
			var itemThumbnail = cdm_public + '/utils/getthumbnail/collection/' + results['records'][i]['collection'].substring(1) + '/id/' + results['records'][i]['pointer'];
			var fullValues = getScaledLink(imgwidth, imgheight, 'full');
			var itemFullsize = cdm_public + '/utils/ajaxhelper/?CISOROOT=' + results['records'][i]['collection'].substring(1) + '&CISOPTR=' + results['records'][i]['pointer'] + '&action=2&DMSCALE=' + fullValues.dmscale + '&DMWIDTH=' + fullValues.dmwidth + '&DMHEIGHT=' + fullValues.dmheight;
			var previewValues = getScaledLink(imgwidth, imgheight, 'preview');
			var itemPreviewLarge = cdm_public + '/utils/ajaxhelper/?CISOROOT=' + results['records'][i]['collection'].substring(1) + '&CISOPTR=' + results['records'][i]['pointer'] + '&action=2&DMSCALE=' + previewValues.dmscale + '&DMWIDTH=' + previewValues.dmwidth + '&DMHEIGHT=' + previewValues.dmheight;

			entry +=	'<div class="list-item">' + 
							'<div class="top">' +
									'<a href="#TB_inline?&height=700&width=620&inlineId=lrgImg' + results['records'][i]['pointer'] + '" class="thickbox" title="Image Detail">'+
									'<div class="img-container" style="background-image:url(' + itemThumbnail + ');">'+

									'</div>' +
                            	'<p class="model">' + results['records'][i]['title'] + '<br></p></a>' +
							'</div>' +
							'<div id="lrgImg' + results['records'][i]['pointer'] + '" style="display:none;">' +
								'<div class="downloadlinks">' + 
									'<input id="imageTitle" type="hidden" value="' + encodeURI(results['records'][i]['title']).replace(/'/g, '%27' ) + '">' +
									'<input id="imageDescription" type="hidden" value="' + encodeURI(results['records'][i]['descri']).replace(/'/g, '%27' ) + '">' +
									'<input id="itemSourceURL" type="hidden" value="' + encodeURI(itemSourceURL) + '">' +


									'<a class="saveImage" href="' + encodeURI(itemFullsize) + '" target="_blank">Save and Insert</a>' +
									'<a class="view-in-cdm" href="' + encodeURI(itemSourceURL) + '" target="_blank">View in CONTENTdm</a>' +
								'</div>' +
								'<div id="mediumView">'+
								'<div class="img-container" style="background-image:url(' + encodeURI(itemPreviewLarge) + ');"></div>'+
								'<div>' + results['records'][i]['descri'] + '</div>'+
								'</div>' +
							'</div>' +
						'</div>';
		}
		if(total>0){
			jQuery('#list-total').html(total+' Results').addClass('active');
			jQuery('.list').addClass('active').html(entry);
		}else{
			jQuery('#list-total').html('No results. Try again.').addClass('active');
			jQuery('.list').removeClass('active')
		}
		
        
        jQuery('.saveImage').click(function(e) {
	        
	        $(this).addClass('working');
        	
        	e.preventDefault();
			var img_url = $(this).attr('href');
			var cdm_filename = decodeURI($(this).siblings().first().val()).replace(/[^A-Za-z0-9 ]/, '');
			var itemTitle=decodeURI(jQuery(this).siblings().first().val()).replace(/%27/g, "'");
			var itemDescription = decodeURI(jQuery(this).siblings().first().next().val()).replace(/%27/g, "'");
			var cdmURI = decodeURI(jQuery(this).siblings().first().next().next().val());
			var itemSourceURI = '&nbsp; <a class="cdm_source" href="'+cdmURI+'" target="_blank">[Source]</a>';

			var data = {
				action: 'cdm_save_image',
				wpnonce: cdm_security_nonce.security,
				src: img_url,
				post_id: cur_post_id,
				filename: cdm_filename,
				keyword: "",
				title: itemTitle,
				description: itemDescription,
				caption: itemDescription+itemSourceURI
			};
			jQuery.ajax ({
				type: 'POST',
				url: ajaxurl,
				data: data,
				dataType: 'json',
				success	: function(response) {
							
					if(response.error != undefined && response.error != "") {
						//console.log(response.error);
						tb_remove();
					} else {
						imgurl = response.result;
						var imgBlock =	'<div class="imgblock cdm_container">'+
										'<figure class="wp-caption alignnone">' + 
											'<img class="size-full" src="' + imgurl + '" alt="'+itemTitle+'"><br/>' + 
											'<figcaption class="wp-caption-text"><div class="cdm_description">' + 
												'<div class="cdm_title"><strong>' + itemTitle + '</strong></div>' +
												itemDescription + 
												itemSourceURI +
											'</div></figcaption>' +
										'</figure>'+
										'</div>';
						
						if(jQuery("#content").is(":visible")) {
							document.getElementById('content').value += imgBlock;
						} else {
							tinyMCE.activeEditor.selection.select(tinyMCE.activeEditor.getBody(), true);
							tinyMCE.activeEditor.selection.collapse(false);
							tinyMCE.execCommand('mceInsertContent',false,imgBlock);
						}
						tb_remove();
						jQuery('html, body').animate({
						    scrollTop: jQuery("#post-body-content").offset().top
						}, 1000, function() {
						    return false;
						});						
						
					}
					
				},
		        error: function() {
		            //console.log("Error"); 
		            tb_remove();         
		        }
			});
			
		});
		
	}
	
    
});